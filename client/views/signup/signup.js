Template.Signup.events({
   'submit form': function (e) {
       e.preventDefault();
       let email = $('[name=email]').val();
       let password = $('[name=password]').val();
       Accounts.createUser({
           email: email,
           password: password
       }, function (error) {
           if (error){
               let alertMessage = document.getElementById('alert');
               alertMessage.innerHTML = error.reason;
               alertMessage.style.display = 'block';
           }
           else {
               Router.go('Successful');
           }
       });
   }
});