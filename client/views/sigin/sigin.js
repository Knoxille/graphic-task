Template.Sigin.events({
    'submit form': function(event){
        event.preventDefault();
        let email = $('[name=email]').val();
        let password = $('[name=password]').val();
        Meteor.loginWithPassword(email, password, function(error){
            if(error){
                let alertMessage = document.getElementById('alert');
                alertMessage.innerHTML = error.reason;
                alertMessage.style.display = 'block';
            }
            else {
                Router.go('Graphics');
            }
        });
    }
});